/********************************************************************************
** Form generated from reading UI file 'form_stats.ui'
**
** Created: Tue 3. May 14:02:54 2016
**      by: Qt User Interface Compiler version 4.8.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_FORM_STATS_H
#define UI_FORM_STATS_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QCalendarWidget>
#include <QtGui/QCheckBox>
#include <QtGui/QFrame>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QLineEdit>
#include <QtGui/QPushButton>
#include <QtGui/QRadioButton>
#include <QtGui/QWidget>
#include "qcustomplot.h"

QT_BEGIN_NAMESPACE

class Ui_Form_stats
{
public:
    QLineEdit *lineEdit_dateFin;
    QLabel *label_moyenne;
    QCheckBox *checkBox_periode;
    QLabel *label_apiAuto;
    QFrame *line;
    QPushButton *btn_genererStats;
    QPushButton *btn_exporter;
    QCheckBox *checkBox_journee;
    QRadioButton *radioButton_reconciliationManu;
    QLabel *label;
    QRadioButton *radioButton_reconciliationAuto;
    QCalendarWidget *calendrier;
    QLabel *label_apiManu;
    QLineEdit *lineEdit_dateDebut;
    QLabel *label_titre;
    QCustomPlot *widget_stats;
    QRadioButton *radioButton_rechercheManu;
    QLineEdit *lineEdit_moyenne;
    QRadioButton *radioButton_rechercheAuto;

    void setupUi(QWidget *Form_stats)
    {
        if (Form_stats->objectName().isEmpty())
            Form_stats->setObjectName(QString::fromUtf8("Form_stats"));
        Form_stats->resize(955, 761);
        lineEdit_dateFin = new QLineEdit(Form_stats);
        lineEdit_dateFin->setObjectName(QString::fromUtf8("lineEdit_dateFin"));
        lineEdit_dateFin->setGeometry(QRect(330, 210, 113, 20));
        label_moyenne = new QLabel(Form_stats);
        label_moyenne->setObjectName(QString::fromUtf8("label_moyenne"));
        label_moyenne->setGeometry(QRect(530, 280, 61, 16));
        checkBox_periode = new QCheckBox(Form_stats);
        checkBox_periode->setObjectName(QString::fromUtf8("checkBox_periode"));
        checkBox_periode->setGeometry(QRect(190, 100, 70, 17));
        label_apiAuto = new QLabel(Form_stats);
        label_apiAuto->setObjectName(QString::fromUtf8("label_apiAuto"));
        label_apiAuto->setGeometry(QRect(550, 150, 91, 20));
        line = new QFrame(Form_stats);
        line->setObjectName(QString::fromUtf8("line"));
        line->setGeometry(QRect(650, 150, 20, 81));
        line->setFrameShape(QFrame::VLine);
        line->setFrameShadow(QFrame::Sunken);
        btn_genererStats = new QPushButton(Form_stats);
        btn_genererStats->setObjectName(QString::fromUtf8("btn_genererStats"));
        btn_genererStats->setGeometry(QRect(710, 280, 121, 23));
        btn_exporter = new QPushButton(Form_stats);
        btn_exporter->setObjectName(QString::fromUtf8("btn_exporter"));
        btn_exporter->setGeometry(QRect(840, 280, 91, 23));
        checkBox_journee = new QCheckBox(Form_stats);
        checkBox_journee->setObjectName(QString::fromUtf8("checkBox_journee"));
        checkBox_journee->setGeometry(QRect(110, 100, 70, 17));
        radioButton_reconciliationManu = new QRadioButton(Form_stats);
        radioButton_reconciliationManu->setObjectName(QString::fromUtf8("radioButton_reconciliationManu"));
        radioButton_reconciliationManu->setGeometry(QRect(680, 210, 91, 17));
        label = new QLabel(Form_stats);
        label->setObjectName(QString::fromUtf8("label"));
        label->setGeometry(QRect(370, 190, 21, 16));
        radioButton_reconciliationAuto = new QRadioButton(Form_stats);
        radioButton_reconciliationAuto->setObjectName(QString::fromUtf8("radioButton_reconciliationAuto"));
        radioButton_reconciliationAuto->setGeometry(QRect(550, 210, 91, 17));
        calendrier = new QCalendarWidget(Form_stats);
        calendrier->setObjectName(QString::fromUtf8("calendrier"));
        calendrier->setGeometry(QRect(40, 130, 256, 155));
        label_apiManu = new QLabel(Form_stats);
        label_apiManu->setObjectName(QString::fromUtf8("label_apiManu"));
        label_apiManu->setGeometry(QRect(690, 150, 71, 20));
        lineEdit_dateDebut = new QLineEdit(Form_stats);
        lineEdit_dateDebut->setObjectName(QString::fromUtf8("lineEdit_dateDebut"));
        lineEdit_dateDebut->setGeometry(QRect(330, 160, 113, 20));
        label_titre = new QLabel(Form_stats);
        label_titre->setObjectName(QString::fromUtf8("label_titre"));
        label_titre->setGeometry(QRect(400, 40, 131, 16));
        widget_stats = new QCustomPlot(Form_stats);
        widget_stats->setObjectName(QString::fromUtf8("widget_stats"));
        widget_stats->setGeometry(QRect(30, 310, 901, 421));
        radioButton_rechercheManu = new QRadioButton(Form_stats);
        radioButton_rechercheManu->setObjectName(QString::fromUtf8("radioButton_rechercheManu"));
        radioButton_rechercheManu->setGeometry(QRect(680, 180, 81, 17));
        lineEdit_moyenne = new QLineEdit(Form_stats);
        lineEdit_moyenne->setObjectName(QString::fromUtf8("lineEdit_moyenne"));
        lineEdit_moyenne->setGeometry(QRect(590, 280, 113, 20));
        radioButton_rechercheAuto = new QRadioButton(Form_stats);
        radioButton_rechercheAuto->setObjectName(QString::fromUtf8("radioButton_rechercheAuto"));
        radioButton_rechercheAuto->setGeometry(QRect(550, 180, 71, 17));

        retranslateUi(Form_stats);

        QMetaObject::connectSlotsByName(Form_stats);
    } // setupUi

    void retranslateUi(QWidget *Form_stats)
    {
        Form_stats->setWindowTitle(QApplication::translate("Form_stats", "Form", 0, QApplication::UnicodeUTF8));
        label_moyenne->setText(QApplication::translate("Form_stats", "Moyenne : ", 0, QApplication::UnicodeUTF8));
        checkBox_periode->setText(QApplication::translate("Form_stats", "P\303\251riode", 0, QApplication::UnicodeUTF8));
        label_apiAuto->setText(QApplication::translate("Form_stats", "API Automatique", 0, QApplication::UnicodeUTF8));
        btn_genererStats->setText(QApplication::translate("Form_stats", "G\303\251n\303\251rer statistique", 0, QApplication::UnicodeUTF8));
        btn_exporter->setText(QApplication::translate("Form_stats", "Exporter", 0, QApplication::UnicodeUTF8));
        checkBox_journee->setText(QApplication::translate("Form_stats", "Journ\303\251e", 0, QApplication::UnicodeUTF8));
        radioButton_reconciliationManu->setText(QApplication::translate("Form_stats", "R\303\251conciliation", 0, QApplication::UnicodeUTF8));
        label->setText(QApplication::translate("Form_stats", "Au", 0, QApplication::UnicodeUTF8));
        radioButton_reconciliationAuto->setText(QApplication::translate("Form_stats", "R\303\251conciliation", 0, QApplication::UnicodeUTF8));
        label_apiManu->setText(QApplication::translate("Form_stats", "API Manuelle", 0, QApplication::UnicodeUTF8));
        label_titre->setText(QApplication::translate("Form_stats", "Statistique r\303\251conciliation", 0, QApplication::UnicodeUTF8));
        radioButton_rechercheManu->setText(QApplication::translate("Form_stats", "Recherche", 0, QApplication::UnicodeUTF8));
        radioButton_rechercheAuto->setText(QApplication::translate("Form_stats", "Recherche", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class Form_stats: public Ui_Form_stats {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_FORM_STATS_H
