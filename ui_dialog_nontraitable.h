/********************************************************************************
** Form generated from reading UI file 'dialog_nontraitable.ui'
**
** Created: Tue 9. Feb 18:24:30 2016
**      by: Qt User Interface Compiler version 4.8.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_DIALOG_NONTRAITABLE_H
#define UI_DIALOG_NONTRAITABLE_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QPushButton>
#include <QtGui/QRadioButton>
#include <QtGui/QTextEdit>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_Dialog_nonTraitable
{
public:
    QPushButton *btn_valider;
    QPushButton *btn_annuler;
    QRadioButton *radioButton_rpecCoupee;
    QRadioButton *radioButton_ChManquant;
    QRadioButton *radioButton_dateEntreeManquante;
    QRadioButton *radioButton_inforsManquantes;
    QRadioButton *radioButton_autres;
    QRadioButton *radioButton_mauvaisCH;
    QRadioButton *radioButton_retourIllisible;
    QLabel *label;
    QLabel *label_2;
    QRadioButton *radioButton_retourDPEC;
    QTextEdit *textEdit;

    void setupUi(QWidget *Dialog_nonTraitable)
    {
        if (Dialog_nonTraitable->objectName().isEmpty())
            Dialog_nonTraitable->setObjectName(QString::fromUtf8("Dialog_nonTraitable"));
        Dialog_nonTraitable->resize(354, 306);
        btn_valider = new QPushButton(Dialog_nonTraitable);
        btn_valider->setObjectName(QString::fromUtf8("btn_valider"));
        btn_valider->setGeometry(QRect(100, 270, 75, 23));
        btn_annuler = new QPushButton(Dialog_nonTraitable);
        btn_annuler->setObjectName(QString::fromUtf8("btn_annuler"));
        btn_annuler->setGeometry(QRect(200, 270, 75, 23));
        radioButton_rpecCoupee = new QRadioButton(Dialog_nonTraitable);
        radioButton_rpecCoupee->setObjectName(QString::fromUtf8("radioButton_rpecCoupee"));
        radioButton_rpecCoupee->setGeometry(QRect(40, 70, 161, 17));
        radioButton_ChManquant = new QRadioButton(Dialog_nonTraitable);
        radioButton_ChManquant->setObjectName(QString::fromUtf8("radioButton_ChManquant"));
        radioButton_ChManquant->setGeometry(QRect(240, 130, 91, 17));
        radioButton_dateEntreeManquante = new QRadioButton(Dialog_nonTraitable);
        radioButton_dateEntreeManquante->setObjectName(QString::fromUtf8("radioButton_dateEntreeManquante"));
        radioButton_dateEntreeManquante->setGeometry(QRect(40, 100, 151, 17));
        radioButton_inforsManquantes = new QRadioButton(Dialog_nonTraitable);
        radioButton_inforsManquantes->setObjectName(QString::fromUtf8("radioButton_inforsManquantes"));
        radioButton_inforsManquantes->setGeometry(QRect(40, 130, 151, 17));
        radioButton_autres = new QRadioButton(Dialog_nonTraitable);
        radioButton_autres->setObjectName(QString::fromUtf8("radioButton_autres"));
        radioButton_autres->setGeometry(QRect(40, 160, 82, 17));
        radioButton_mauvaisCH = new QRadioButton(Dialog_nonTraitable);
        radioButton_mauvaisCH->setObjectName(QString::fromUtf8("radioButton_mauvaisCH"));
        radioButton_mauvaisCH->setGeometry(QRect(240, 70, 91, 17));
        radioButton_retourIllisible = new QRadioButton(Dialog_nonTraitable);
        radioButton_retourIllisible->setObjectName(QString::fromUtf8("radioButton_retourIllisible"));
        radioButton_retourIllisible->setGeometry(QRect(240, 100, 82, 17));
        label = new QLabel(Dialog_nonTraitable);
        label->setObjectName(QString::fromUtf8("label"));
        label->setGeometry(QRect(10, 40, 151, 21));
        label_2 = new QLabel(Dialog_nonTraitable);
        label_2->setObjectName(QString::fromUtf8("label_2"));
        label_2->setGeometry(QRect(150, 10, 71, 16));
        radioButton_retourDPEC = new QRadioButton(Dialog_nonTraitable);
        radioButton_retourDPEC->setObjectName(QString::fromUtf8("radioButton_retourDPEC"));
        radioButton_retourDPEC->setGeometry(QRect(240, 160, 82, 17));
        textEdit = new QTextEdit(Dialog_nonTraitable);
        textEdit->setObjectName(QString::fromUtf8("textEdit"));
        textEdit->setGeometry(QRect(40, 190, 291, 71));

        retranslateUi(Dialog_nonTraitable);

        QMetaObject::connectSlotsByName(Dialog_nonTraitable);
    } // setupUi

    void retranslateUi(QWidget *Dialog_nonTraitable)
    {
        Dialog_nonTraitable->setWindowTitle(QApplication::translate("Dialog_nonTraitable", "Form", 0, QApplication::UnicodeUTF8));
        btn_valider->setText(QApplication::translate("Dialog_nonTraitable", "Valider", 0, QApplication::UnicodeUTF8));
        btn_annuler->setText(QApplication::translate("Dialog_nonTraitable", "Annuler", 0, QApplication::UnicodeUTF8));
        radioButton_rpecCoupee->setText(QApplication::translate("Dialog_nonTraitable", "RPEC framgent\303\251 / d\303\251coup\303\251", 0, QApplication::UnicodeUTF8));
        radioButton_ChManquant->setText(QApplication::translate("Dialog_nonTraitable", "C.H manquant", 0, QApplication::UnicodeUTF8));
        radioButton_dateEntreeManquante->setText(QApplication::translate("Dialog_nonTraitable", "Date d'entr\303\251e manquante", 0, QApplication::UnicodeUTF8));
        radioButton_inforsManquantes->setText(QApplication::translate("Dialog_nonTraitable", "Informations manquantes", 0, QApplication::UnicodeUTF8));
        radioButton_autres->setText(QApplication::translate("Dialog_nonTraitable", "Autres :", 0, QApplication::UnicodeUTF8));
        radioButton_mauvaisCH->setText(QApplication::translate("Dialog_nonTraitable", "Mauvais C.H", 0, QApplication::UnicodeUTF8));
        radioButton_retourIllisible->setText(QApplication::translate("Dialog_nonTraitable", "RPEC illisible", 0, QApplication::UnicodeUTF8));
        label->setText(QApplication::translate("Dialog_nonTraitable", "Veuillez s\303\251lectionner un motif :", 0, QApplication::UnicodeUTF8));
        label_2->setText(QApplication::translate("Dialog_nonTraitable", "Non traitable", 0, QApplication::UnicodeUTF8));
        radioButton_retourDPEC->setText(QApplication::translate("Dialog_nonTraitable", "Retour DPEC", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class Dialog_nonTraitable: public Ui_Dialog_nonTraitable {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_DIALOG_NONTRAITABLE_H
