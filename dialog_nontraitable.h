#ifndef DIALOG_NONTRAITABLE_H
#define DIALOG_NONTRAITABLE_H

#include <QWidget>

namespace Ui {
class Dialog_nonTraitable;
}

class Dialog_nonTraitable : public QWidget
{
    Q_OBJECT
    
public:
    explicit Dialog_nonTraitable(QWidget *parent = 0);
    ~Dialog_nonTraitable();
    
private slots:
    void on_btn_valider_clicked();

    void on_btn_annuler_clicked();

    void on_radioButton_autres_clicked();

signals:
    void on_motifValider(QString motif);

private:
    Ui::Dialog_nonTraitable *ui;
};

#endif // DIALOG_NONTRAITABLE_H
