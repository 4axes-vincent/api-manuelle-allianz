#include "Configuration.h"

Configuration::Configuration()
{
}




/*******************************************************************/
/********** GETTEUR CONFIG.INI POUR LA BASE DE DONNEE **************/
/*******************************************************************/


//----------------------------------------------------------------------------------------------------------------------
//Getteur -> Chemin d'acc�s du fichier Config.ini du SuperViseur
QString Configuration::getPathINI(){

    QSettings settings( QCoreApplication::applicationDirPath () + "\\config.ini", QSettings::IniFormat);
    QString pathConfig = settings.value("Path/config","Default value path config.ini").toString();
    return(pathConfig);
}



















//----------------------------------------------------------------------------------------------------------------------
/*
  Renvoi le pr�nom et le nom de l'op�ratrice connect�e
  */
QString Configuration::getUserCourant (){

    QSettings settings( getPathINI() + "\\config.ini", QSettings::IniFormat );
    QString user = settings.value("Identification/User","Default value path/configSPV").toString();

    return(user);

}




















//----------------------------------------------------------------------------------------------------------------------
//Getteur -> Nom de la base de donn�es
QString Configuration::getDatabase(){

    QSettings settings( getPathINI() + "\\config.ini", QSettings::IniFormat);
    QString database = settings.value("BDD/database","Default value BDD database").toString();
    return(database);
}


















//----------------------------------------------------------------------------------------------------------------------
//Getteur -> host name
QString Configuration::getHostName(){

    QSettings settings( getPathINI() + "\\config.ini", QSettings::IniFormat);
    QString hostName = settings.value("BDD/hostName","Default value path analyseur").toString();
    return(hostName);

}




















//----------------------------------------------------------------------------------------------------------------------
//Getteur -> userName BDD
QString Configuration::getUserName(){

    QSettings settings( getPathINI() + "\\config.ini", QSettings::IniFormat);
    QString userName = settings.value("BDD/userName","Default value path analyseur").toString();
    return(userName);

}
























//----------------------------------------------------------------------------------------------------------------------
//Getteur -> password BDD
QString Configuration::getPassWord(){


    QSettings settings( getPathINI() + "\\config.ini", QSettings::IniFormat);
    QString passWord = settings.value("BDD/passWord","Default value path analyseur").toString();
    return(passWord);
}














//----------------------------------------------------------------------------------------------------------------------
/*
  Renvoi le nom de la base de donn�es (fichier)
  */
QString Configuration::getDataBaseName (){

    return( getPathINI () + "\\" + QHostInfo::localHostName() );
}
















//----------------------------------------------------------------------------------------------------------------------
/*
  Permet de retourner la liste de cl� du groupe [groupe] dans le fichier de configuration [fichierIni]
  */
QStringList Configuration::getListKey(QString fichierIni, QString groupe){

     QSettings settings( fichierIni , QSettings::IniFormat);
     settings.beginGroup (groupe);
     return( settings.allKeys () );
}














//----------------------------------------------------------------------------------------------------------------------
/*
  Permet de retourner la valeur de cl� [key] dans le groupe [groupe] du fichier de configuration [fichierIni]
  */
QString Configuration::getValue(QString fichierIni, QString groupe, QString key){

    QSettings settings( fichierIni , QSettings::IniFormat);
    settings.beginGroup (groupe);
    return( settings.value ( key, "-1").toString () );
}












//----------------------------------------------------------------------------------------------------------------------
/*
  Permet d'indiquer s'il y a un processus SuperViseur.exe en cours
  TRUE  -> Le processus n'est pas en route
  FALSE -> Le processus est en cours d'ex�cution
  */
bool Configuration::getCrashSuperViseur(){


    //On cherche le processus "SuperViseur.exe"
    QProcess p;
    QByteArray output;


    p.setProcessChannelMode(QProcess::MergedChannels);
    p.start ("cmd.exe /C TaskList | findstr /c:SuperViseur.exe");
    p.waitForFinished ();
    output = p.readAllStandardOutput ();

    //S'il y a un r�sultat et config OK
    if( output.isEmpty () )
        return(true);

    return(false);
}
