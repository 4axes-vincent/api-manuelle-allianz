#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QMessageBox>
#include <QStringList>
#include <QFileInfo>
#include <QFile>
#include <QTextStream>
#include <QStringBuilder>
#include <QDateTime>

#include <QtNetwork/QNetworkAccessManager>
#include <QtNetwork/QNetworkRequest>
#include <QtNetwork/QNetworkReply>
#include <QtNetwork/QNetworkProxy>

#include <QtXml/QDomDocument>
#include <QtXml/QXmlStreamReader>

#include <QTimer>
#include <QProcess>
#include <QDir>
#include <QList>
#include <QListWidgetItem>
#include <QCheckBox>
#include <QDesktopServices>
#include <QHostInfo>
#include <windows.h>
#include <QTime>
#include <QKeyEvent>
#include <QDesktopWidget>
#include <QtWebKit>
#include <QPrinter>
#include <QCompleter>
#include <QIcon>

#include <QtNetwork/QTcpSocket>
#include <QtNetwork/QSsl>
#include <QDataStream>

#include "dial_bdd.h"
#include "dialog_lecteurpdf.h"
#include "dialog_nontraitable.h"


namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

    struct statutMessage{

        QString acquitte;
        QString remis;
        QString purge;
        QString annule;
    };

    struct contexte{

        QString recherche;
        QString simple;
        QString multiple;
        QString SEC;
        QString statutRPEC;
        QString switchRPEC;
    };

    struct DPEC{

        QString idMessageDPEC;
        QString idMessageRPEC;
        QString statutDPEC;
        QString statutRPEC;
        QString emetteur;
        QString idClientE;
        QString destinataire;
        QString idClientD;
        QString dateEntree;
        QString prenomPatient;
        QString nomPatient;
        QString numDossier;
    };


    struct structurePopper{

        QString good;
        QString unknown;
    };

    struct zoneErreur{

        QString insertEchoue;
        QString reponseEmise;
        QString mauvaisRNM;
    };
    
public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

    void debugger(QString titre, QString info);
    void debuggerInfo (QString titre, QString info);
    void ecrireLog(QString nomFichier, QString info);

    void lancerRequete(QFileInfo informationFichier, QString rnmMutuelle);
    void parserReponse(QString reponse, QString contexte);
    bool deplacerFichier(QString dossierDestination, bool sousRepertoire);

    void envoyerFichierSimple(QString idMessage, QString rnmEnCours);
    void envoyerFichierMultiple(QStringList listeIdMessage, QString rnmEnCours);
    void envoyerFichierEnSec (QString idEDS, QString rnmEnCours);
    void getStatutRPEC(QString idMessage);

    void remplirTableau(QList<DPEC> listeStruct);
    void remplirTableauCheckBox(QList<DPEC> listeStruct, bool checked);
    void compterDPECSelectionner(QStringList &listeDPECARecon);
    bool verifierStatut(QStringList listeDPECRecon, QList<DPEC> listeStruct);

    void envoyerRecherche(QString NNI, QString nomPatient, QString numDossier, QString mutuelle, QString eds);
    void testReponse();
    QString getIdMsgRPEC(QString idMessageDPEC, QList<DPEC> listeStruct);

    void listerFichier(QString path, bool checkBox);
    void importationLocalEDS( QString nomFichierEDS );
    void envoyerSwitch(QString idRPEC);

    void listerTMP( QString path );
    void listerVM();
    QString getIdEmetteur( QString idMessageDPEC );

    QString getNomMutuelle( QString idMessageDPEC );
    void compterFichierSelectionner( QStringList &listeFichierSelectionner );
    bool suppressionFichier( QString fichierASupprimer );

    bool deplacementFichier( QString dossier, QString fichier);
    void fermerFichierPDF();
    QString getLecteurPDFDefaut();

    void enregistrerMotif(QString libelleMotif);
    void archiverFichierMotif( QString pathFichierMotif );
    void envoyerEmail(QString pathPDF, QString motif);


    
private slots:
    void on_btn_executer_clicked();

    void on_btn_rechercher_clicked();

    void retour_recherche(QNetworkReply *);

    void on_listWidget_DPEC_itemClicked(QListWidgetItem *item);

    void on_checkBox_multiReconciliation_clicked();

    void retour_telechargerFichier(QNetworkReply * reply);

    void retour_telechargerFichierEDS(QNetworkReply * reply);

    void on_btn_telechargerRPEC_clicked();

    void retour_getStatutRPEC(QNetworkReply * reply);

    void on_btn_importerEDS_clicked();

    void on_btn_telechargerDPEC_clicked();

    void keyPressEvent (QKeyEvent *e);

    void progressionTelechargement(qint64 bytesReceived, qint64 bytesTotal);

    void messageErreur(QNetworkReply::NetworkError);

    void on_listWidget_fichierPDF_itemClicked(QListWidgetItem *item);

    void on_btn_deplacerTMP_clicked();

    void deplacerFichierTMP( QString TMPDestination );

    void viderWidget();

    void on_btn_supprimer_clicked();

    void on_btn_ouvrirDossier_clicked();

    void on_btn_actualiserPDF_clicked();

    void on_listWidget_DPEC_currentTextChanged(const QString &currentText);

    void on_radioButton_switch_clicked();

    void on_radioButton_reconcilier_clicked();

    void on_radioButton_erreurDest_clicked();

    void on_radioButton_envoyerSEC_clicked();

    void on_radioButton_ArchivageNR_clicked();

    void on_btn_signalErreur_clicked();

    void on_checkBox_multiSelection_clicked();

    void on_listWidget_fichierPDF_itemDoubleClicked(QListWidgetItem *item);

    void on_actionLecteur_PDF_triggered();

    void on_btn_decouperPDF_clicked();

    void on_checkBox_selectionnerTout_clicked();

    void archiverNT(QString motif);

    void on_btn_importerAutresMutuelles_clicked();

    void on_actionMise_jour_triggered();

private:
    Ui::MainWindow *ui;

    QString idMsgSelect;
    QString idEDSSelect;
    QString pdfSelect;

    QString pathFichier;
    QString mutuelleEnCours;
    QString nomFichier;
    QString rnm_courant;
    QString idMutuellePlateforme;
    QString hostname;

    QString pathMutuelleEnCours;

    QTime *chrono;
    QTime *time;
    QDateTime *dateTime;

    QStringList listeEDS;
    QList<DPEC> listeDPEC;

    contexte typeContexte;
    statutMessage StatutDPEC;
    structurePopper structPopper;
    zoneErreur structErreur;

    Configuration *config;

    bool importationEDS;
    bool siVmImporter;

};

#endif // MAINWINDOW_H
