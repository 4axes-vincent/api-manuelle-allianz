#ifndef DIALOG_LECTEURPDF_H
#define DIALOG_LECTEURPDF_H

#include <QDialog>
#include <QSettings>
#include <QString>
#include <QCoreApplication>
#include <QStringBuilder>
#include <QMessageBox>

namespace Ui {
class Dialog_lecteurPDF;
}

class Dialog_lecteurPDF : public QDialog
{
    Q_OBJECT
    
public:
    explicit Dialog_lecteurPDF(QWidget *parent = 0);
    ~Dialog_lecteurPDF();

    void initialiser();
    void setLecteurPDFDefaut(QString lecteurPDF);
    
private slots:
    void on_buttonBox_accepted();

private:
    Ui::Dialog_lecteurPDF *ui;
};

#endif // DIALOG_LECTEURPDF_H
