/********************************************************************************
** Form generated from reading UI file 'dialog_lecteurpdf.ui'
**
** Created: Thu 5. Nov 13:58:54 2015
**      by: Qt User Interface Compiler version 4.8.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_DIALOG_LECTEURPDF_H
#define UI_DIALOG_LECTEURPDF_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QDialog>
#include <QtGui/QDialogButtonBox>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QRadioButton>

QT_BEGIN_NAMESPACE

class Ui_Dialog_lecteurPDF
{
public:
    QDialogButtonBox *buttonBox;
    QRadioButton *radioButton_adobeReader;
    QRadioButton *radioButton_sumatraPDF;
    QLabel *label_titreLecteurPDF;

    void setupUi(QDialog *Dialog_lecteurPDF)
    {
        if (Dialog_lecteurPDF->objectName().isEmpty())
            Dialog_lecteurPDF->setObjectName(QString::fromUtf8("Dialog_lecteurPDF"));
        Dialog_lecteurPDF->resize(249, 125);
        buttonBox = new QDialogButtonBox(Dialog_lecteurPDF);
        buttonBox->setObjectName(QString::fromUtf8("buttonBox"));
        buttonBox->setGeometry(QRect(40, 70, 161, 32));
        buttonBox->setOrientation(Qt::Horizontal);
        buttonBox->setStandardButtons(QDialogButtonBox::Cancel|QDialogButtonBox::Ok);
        radioButton_adobeReader = new QRadioButton(Dialog_lecteurPDF);
        radioButton_adobeReader->setObjectName(QString::fromUtf8("radioButton_adobeReader"));
        radioButton_adobeReader->setGeometry(QRect(30, 40, 101, 17));
        radioButton_sumatraPDF = new QRadioButton(Dialog_lecteurPDF);
        radioButton_sumatraPDF->setObjectName(QString::fromUtf8("radioButton_sumatraPDF"));
        radioButton_sumatraPDF->setGeometry(QRect(150, 40, 82, 17));
        label_titreLecteurPDF = new QLabel(Dialog_lecteurPDF);
        label_titreLecteurPDF->setObjectName(QString::fromUtf8("label_titreLecteurPDF"));
        label_titreLecteurPDF->setGeometry(QRect(70, 10, 121, 20));

        retranslateUi(Dialog_lecteurPDF);
        QObject::connect(buttonBox, SIGNAL(accepted()), Dialog_lecteurPDF, SLOT(accept()));
        QObject::connect(buttonBox, SIGNAL(rejected()), Dialog_lecteurPDF, SLOT(reject()));

        QMetaObject::connectSlotsByName(Dialog_lecteurPDF);
    } // setupUi

    void retranslateUi(QDialog *Dialog_lecteurPDF)
    {
        Dialog_lecteurPDF->setWindowTitle(QApplication::translate("Dialog_lecteurPDF", "Dialog", 0, QApplication::UnicodeUTF8));
        radioButton_adobeReader->setText(QApplication::translate("Dialog_lecteurPDF", "AdobeReader", 0, QApplication::UnicodeUTF8));
        radioButton_sumatraPDF->setText(QApplication::translate("Dialog_lecteurPDF", "SumatraPDF", 0, QApplication::UnicodeUTF8));
        label_titreLecteurPDF->setText(QApplication::translate("Dialog_lecteurPDF", "Lecteur PDF par d\303\251faut", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class Dialog_lecteurPDF: public Ui_Dialog_lecteurPDF {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_DIALOG_LECTEURPDF_H
