#include "dialog_nontraitable.h"
#include "ui_dialog_nontraitable.h"

Dialog_nonTraitable::Dialog_nonTraitable(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Dialog_nonTraitable)
{
    ui->setupUi(this);
    this->setFixedSize (354,306);
    this->setWindowTitle ("Motif non traitable");

    ui->textEdit->setVisible ( false );

}

Dialog_nonTraitable::~Dialog_nonTraitable()
{
    delete ui;
}






void Dialog_nonTraitable::on_btn_valider_clicked(){

    if( ui->radioButton_autres->isChecked () )
        emit on_motifValider ( ui->textEdit->toPlainText () );
    else{

        if ( ui->radioButton_ChManquant->isChecked () )
            emit on_motifValider ( ui->radioButton_ChManquant->text () );

        else if ( ui->radioButton_dateEntreeManquante->isChecked () )
            emit on_motifValider ( ui->radioButton_dateEntreeManquante->text () );

        else if ( ui->radioButton_inforsManquantes->isChecked () )
            emit on_motifValider ( ui->radioButton_inforsManquantes->text () );

        else if ( ui->radioButton_mauvaisCH->isChecked () )
            emit on_motifValider ( ui->radioButton_mauvaisCH->text () );

        else if ( ui->radioButton_retourDPEC->isChecked () )
            emit on_motifValider ( ui->radioButton_retourDPEC->text () );

        else if ( ui->radioButton_retourIllisible->isChecked () )
            emit on_motifValider ( ui->radioButton_retourIllisible->text () );

        else if ( ui->radioButton_rpecCoupee->isChecked () )
            emit on_motifValider ( ui->radioButton_rpecCoupee->text ());
    }

    close();

}





void Dialog_nonTraitable::on_btn_annuler_clicked(){

    close();
}





void Dialog_nonTraitable::on_radioButton_autres_clicked(){

    ui->textEdit->setVisible ( true );

}

