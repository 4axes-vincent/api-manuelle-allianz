/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created: Wed 23. Mar 08:18:58 2016
**      by: Qt User Interface Compiler version 4.8.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QCheckBox>
#include <QtGui/QComboBox>
#include <QtGui/QFrame>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QLineEdit>
#include <QtGui/QListWidget>
#include <QtGui/QMainWindow>
#include <QtGui/QMenu>
#include <QtGui/QMenuBar>
#include <QtGui/QProgressBar>
#include <QtGui/QPushButton>
#include <QtGui/QRadioButton>
#include <QtGui/QStatusBar>
#include <QtGui/QTextEdit>
#include <QtGui/QToolBar>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QAction *actionLecteur_PDF;
    QAction *actionMise_jour;
    QWidget *centralWidget;
    QLabel *label_nomFichier;
    QLineEdit *lineEdit_nomFichier;
    QFrame *line;
    QRadioButton *radioButton_reconcilier;
    QRadioButton *radioButton_erreurDest;
    QRadioButton *radioButton_envoyerSEC;
    QRadioButton *radioButton_ArchivageNR;
    QPushButton *btn_executer;
    QFrame *line_2;
    QListWidget *listWidget_DPEC;
    QLabel *label_emetteur_DPEC;
    QLabel *label_dest_DPEC;
    QLabel *label_dateEntree;
    QLabel *label_nomPatient;
    QLabel *label_numDossier;
    QLineEdit *lineEdit_emetteur_DPEC;
    QLineEdit *lineEdit_dest_DPEC;
    QLineEdit *lineEdit_dateEntree;
    QLineEdit *lineEdit_nomPatient;
    QLineEdit *lineEdit_numDossier;
    QFrame *line_3;
    QLabel *label_recherche;
    QLabel *label_IDMsg;
    QLineEdit *lineEdit_IDMsg;
    QLabel *label_NNI;
    QLineEdit *lineEdit_NNI;
    QLineEdit *lineEdit_nomPatient_rche;
    QLabel *label_nomPatient_rche;
    QLineEdit *lineEdit_rnmMutuelle_rche;
    QLabel *label_nomMutuelle;
    QPushButton *btn_rechercher;
    QLabel *label_nomEDS;
    QLineEdit *lineEdit_nomEDS;
    QLineEdit *lineEdit_numDossier_rche;
    QLabel *label_numDossier_rche;
    QLabel *label_statutDPEC;
    QLineEdit *lineEdit_statutDPEC;
    QCheckBox *checkBox_multiReconciliation;
    QTextEdit *zoneTexte;
    QLabel *label_zoneErreur;
    QLabel *label_statutRPEC;
    QLineEdit *lineEdit_statutRPEC;
    QLabel *label_nbDPEC;
    QPushButton *btn_importerEDS;
    QPushButton *btn_telechargerDPEC;
    QPushButton *btn_telechargerRPEC;
    QProgressBar *progressBar;
    QLabel *label_prenomPatient;
    QLineEdit *lineEdit_prenomPatient;
    QLabel *label_tempsRequete;
    QFrame *line_4;
    QListWidget *listWidget_fichierPDF;
    QPushButton *btn_deplacerTMP;
    QComboBox *comboBox_TMP;
    QPushButton *btn_supprimer;
    QPushButton *btn_ouvrirDossier;
    QLabel *label_mutuelleEnCours;
    QPushButton *btn_actualiserPDF;
    QCheckBox *checkBox_nonDeplacement;
    QRadioButton *radioButton_switch;
    QLineEdit *lineEdit_mdpSWITCH;
    QPushButton *btn_signalErreur;
    QCheckBox *checkBox_multiSelection;
    QCheckBox *checkBox_selectionnerTout;
    QRadioButton *radioButton_ArchivageNT;
    QComboBox *comboBox_autresMutuelles;
    QPushButton *btn_importerAutresMutuelles;
    QMenuBar *menuBar;
    QMenu *menuEdition;
    QMenu *menuAide;
    QToolBar *mainToolBar;
    QStatusBar *statusBar;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QString::fromUtf8("MainWindow"));
        MainWindow->resize(801, 709);
        actionLecteur_PDF = new QAction(MainWindow);
        actionLecteur_PDF->setObjectName(QString::fromUtf8("actionLecteur_PDF"));
        actionMise_jour = new QAction(MainWindow);
        actionMise_jour->setObjectName(QString::fromUtf8("actionMise_jour"));
        centralWidget = new QWidget(MainWindow);
        centralWidget->setObjectName(QString::fromUtf8("centralWidget"));
        label_nomFichier = new QLabel(centralWidget);
        label_nomFichier->setObjectName(QString::fromUtf8("label_nomFichier"));
        label_nomFichier->setGeometry(QRect(420, 10, 71, 16));
        lineEdit_nomFichier = new QLineEdit(centralWidget);
        lineEdit_nomFichier->setObjectName(QString::fromUtf8("lineEdit_nomFichier"));
        lineEdit_nomFichier->setGeometry(QRect(490, 10, 271, 20));
        line = new QFrame(centralWidget);
        line->setObjectName(QString::fromUtf8("line"));
        line->setGeometry(QRect(330, 30, 461, 21));
        line->setFrameShape(QFrame::HLine);
        line->setFrameShadow(QFrame::Sunken);
        radioButton_reconcilier = new QRadioButton(centralWidget);
        radioButton_reconcilier->setObjectName(QString::fromUtf8("radioButton_reconcilier"));
        radioButton_reconcilier->setGeometry(QRect(366, 520, 82, 17));
        radioButton_erreurDest = new QRadioButton(centralWidget);
        radioButton_erreurDest->setObjectName(QString::fromUtf8("radioButton_erreurDest"));
        radioButton_erreurDest->setGeometry(QRect(366, 550, 121, 17));
        radioButton_envoyerSEC = new QRadioButton(centralWidget);
        radioButton_envoyerSEC->setObjectName(QString::fromUtf8("radioButton_envoyerSEC"));
        radioButton_envoyerSEC->setGeometry(QRect(516, 520, 101, 17));
        radioButton_ArchivageNR = new QRadioButton(centralWidget);
        radioButton_ArchivageNR->setObjectName(QString::fromUtf8("radioButton_ArchivageNR"));
        radioButton_ArchivageNR->setGeometry(QRect(650, 520, 91, 17));
        btn_executer = new QPushButton(centralWidget);
        btn_executer->setObjectName(QString::fromUtf8("btn_executer"));
        btn_executer->setGeometry(QRect(500, 580, 81, 23));
        line_2 = new QFrame(centralWidget);
        line_2->setObjectName(QString::fromUtf8("line_2"));
        line_2->setGeometry(QRect(330, 500, 461, 21));
        line_2->setFrameShape(QFrame::HLine);
        line_2->setFrameShadow(QFrame::Sunken);
        listWidget_DPEC = new QListWidget(centralWidget);
        listWidget_DPEC->setObjectName(QString::fromUtf8("listWidget_DPEC"));
        listWidget_DPEC->setGeometry(QRect(330, 220, 141, 151));
        label_emetteur_DPEC = new QLabel(centralWidget);
        label_emetteur_DPEC->setObjectName(QString::fromUtf8("label_emetteur_DPEC"));
        label_emetteur_DPEC->setGeometry(QRect(490, 290, 81, 16));
        label_dest_DPEC = new QLabel(centralWidget);
        label_dest_DPEC->setObjectName(QString::fromUtf8("label_dest_DPEC"));
        label_dest_DPEC->setGeometry(QRect(490, 320, 101, 16));
        label_dateEntree = new QLabel(centralWidget);
        label_dateEntree->setObjectName(QString::fromUtf8("label_dateEntree"));
        label_dateEntree->setGeometry(QRect(490, 350, 91, 16));
        label_nomPatient = new QLabel(centralWidget);
        label_nomPatient->setObjectName(QString::fromUtf8("label_nomPatient"));
        label_nomPatient->setGeometry(QRect(490, 380, 41, 16));
        label_numDossier = new QLabel(centralWidget);
        label_numDossier->setObjectName(QString::fromUtf8("label_numDossier"));
        label_numDossier->setGeometry(QRect(490, 440, 91, 16));
        lineEdit_emetteur_DPEC = new QLineEdit(centralWidget);
        lineEdit_emetteur_DPEC->setObjectName(QString::fromUtf8("lineEdit_emetteur_DPEC"));
        lineEdit_emetteur_DPEC->setGeometry(QRect(590, 290, 191, 20));
        lineEdit_dest_DPEC = new QLineEdit(centralWidget);
        lineEdit_dest_DPEC->setObjectName(QString::fromUtf8("lineEdit_dest_DPEC"));
        lineEdit_dest_DPEC->setGeometry(QRect(590, 320, 191, 20));
        lineEdit_dateEntree = new QLineEdit(centralWidget);
        lineEdit_dateEntree->setObjectName(QString::fromUtf8("lineEdit_dateEntree"));
        lineEdit_dateEntree->setGeometry(QRect(590, 350, 191, 20));
        lineEdit_nomPatient = new QLineEdit(centralWidget);
        lineEdit_nomPatient->setObjectName(QString::fromUtf8("lineEdit_nomPatient"));
        lineEdit_nomPatient->setGeometry(QRect(590, 380, 191, 20));
        lineEdit_numDossier = new QLineEdit(centralWidget);
        lineEdit_numDossier->setObjectName(QString::fromUtf8("lineEdit_numDossier"));
        lineEdit_numDossier->setGeometry(QRect(590, 440, 191, 20));
        line_3 = new QFrame(centralWidget);
        line_3->setObjectName(QString::fromUtf8("line_3"));
        line_3->setGeometry(QRect(330, 180, 461, 21));
        line_3->setFrameShape(QFrame::HLine);
        line_3->setFrameShadow(QFrame::Sunken);
        label_recherche = new QLabel(centralWidget);
        label_recherche->setObjectName(QString::fromUtf8("label_recherche"));
        label_recherche->setGeometry(QRect(333, 50, 61, 16));
        label_IDMsg = new QLabel(centralWidget);
        label_IDMsg->setObjectName(QString::fromUtf8("label_IDMsg"));
        label_IDMsg->setGeometry(QRect(490, 200, 71, 16));
        lineEdit_IDMsg = new QLineEdit(centralWidget);
        lineEdit_IDMsg->setObjectName(QString::fromUtf8("lineEdit_IDMsg"));
        lineEdit_IDMsg->setGeometry(QRect(590, 200, 191, 20));
        label_NNI = new QLabel(centralWidget);
        label_NNI->setObjectName(QString::fromUtf8("label_NNI"));
        label_NNI->setGeometry(QRect(333, 80, 91, 16));
        lineEdit_NNI = new QLineEdit(centralWidget);
        lineEdit_NNI->setObjectName(QString::fromUtf8("lineEdit_NNI"));
        lineEdit_NNI->setGeometry(QRect(430, 80, 131, 20));
        lineEdit_nomPatient_rche = new QLineEdit(centralWidget);
        lineEdit_nomPatient_rche->setObjectName(QString::fromUtf8("lineEdit_nomPatient_rche"));
        lineEdit_nomPatient_rche->setGeometry(QRect(430, 120, 131, 20));
        label_nomPatient_rche = new QLabel(centralWidget);
        label_nomPatient_rche->setObjectName(QString::fromUtf8("label_nomPatient_rche"));
        label_nomPatient_rche->setGeometry(QRect(333, 120, 81, 16));
        lineEdit_rnmMutuelle_rche = new QLineEdit(centralWidget);
        lineEdit_rnmMutuelle_rche->setObjectName(QString::fromUtf8("lineEdit_rnmMutuelle_rche"));
        lineEdit_rnmMutuelle_rche->setGeometry(QRect(660, 80, 111, 20));
        label_nomMutuelle = new QLabel(centralWidget);
        label_nomMutuelle->setObjectName(QString::fromUtf8("label_nomMutuelle"));
        label_nomMutuelle->setGeometry(QRect(600, 80, 51, 16));
        btn_rechercher = new QPushButton(centralWidget);
        btn_rechercher->setObjectName(QString::fromUtf8("btn_rechercher"));
        btn_rechercher->setGeometry(QRect(610, 160, 81, 23));
        label_nomEDS = new QLabel(centralWidget);
        label_nomEDS->setObjectName(QString::fromUtf8("label_nomEDS"));
        label_nomEDS->setGeometry(QRect(600, 120, 41, 16));
        lineEdit_nomEDS = new QLineEdit(centralWidget);
        lineEdit_nomEDS->setObjectName(QString::fromUtf8("lineEdit_nomEDS"));
        lineEdit_nomEDS->setGeometry(QRect(660, 120, 111, 20));
        lineEdit_numDossier_rche = new QLineEdit(centralWidget);
        lineEdit_numDossier_rche->setObjectName(QString::fromUtf8("lineEdit_numDossier_rche"));
        lineEdit_numDossier_rche->setGeometry(QRect(430, 160, 131, 20));
        label_numDossier_rche = new QLabel(centralWidget);
        label_numDossier_rche->setObjectName(QString::fromUtf8("label_numDossier_rche"));
        label_numDossier_rche->setGeometry(QRect(333, 160, 81, 16));
        label_statutDPEC = new QLabel(centralWidget);
        label_statutDPEC->setObjectName(QString::fromUtf8("label_statutDPEC"));
        label_statutDPEC->setGeometry(QRect(490, 230, 71, 16));
        lineEdit_statutDPEC = new QLineEdit(centralWidget);
        lineEdit_statutDPEC->setObjectName(QString::fromUtf8("lineEdit_statutDPEC"));
        lineEdit_statutDPEC->setGeometry(QRect(590, 230, 81, 20));
        checkBox_multiReconciliation = new QCheckBox(centralWidget);
        checkBox_multiReconciliation->setObjectName(QString::fromUtf8("checkBox_multiReconciliation"));
        checkBox_multiReconciliation->setGeometry(QRect(510, 480, 111, 17));
        zoneTexte = new QTextEdit(centralWidget);
        zoneTexte->setObjectName(QString::fromUtf8("zoneTexte"));
        zoneTexte->setGeometry(QRect(330, 410, 141, 71));
        label_zoneErreur = new QLabel(centralWidget);
        label_zoneErreur->setObjectName(QString::fromUtf8("label_zoneErreur"));
        label_zoneErreur->setGeometry(QRect(370, 390, 71, 16));
        label_statutRPEC = new QLabel(centralWidget);
        label_statutRPEC->setObjectName(QString::fromUtf8("label_statutRPEC"));
        label_statutRPEC->setGeometry(QRect(490, 260, 71, 16));
        lineEdit_statutRPEC = new QLineEdit(centralWidget);
        lineEdit_statutRPEC->setObjectName(QString::fromUtf8("lineEdit_statutRPEC"));
        lineEdit_statutRPEC->setGeometry(QRect(590, 260, 81, 20));
        label_nbDPEC = new QLabel(centralWidget);
        label_nbDPEC->setObjectName(QString::fromUtf8("label_nbDPEC"));
        label_nbDPEC->setGeometry(QRect(340, 200, 121, 16));
        btn_importerEDS = new QPushButton(centralWidget);
        btn_importerEDS->setObjectName(QString::fromUtf8("btn_importerEDS"));
        btn_importerEDS->setGeometry(QRect(710, 160, 81, 23));
        btn_telechargerDPEC = new QPushButton(centralWidget);
        btn_telechargerDPEC->setObjectName(QString::fromUtf8("btn_telechargerDPEC"));
        btn_telechargerDPEC->setGeometry(QRect(680, 230, 101, 23));
        btn_telechargerRPEC = new QPushButton(centralWidget);
        btn_telechargerRPEC->setObjectName(QString::fromUtf8("btn_telechargerRPEC"));
        btn_telechargerRPEC->setGeometry(QRect(680, 260, 101, 23));
        progressBar = new QProgressBar(centralWidget);
        progressBar->setObjectName(QString::fromUtf8("progressBar"));
        progressBar->setGeometry(QRect(330, 610, 461, 20));
        progressBar->setValue(24);
        label_prenomPatient = new QLabel(centralWidget);
        label_prenomPatient->setObjectName(QString::fromUtf8("label_prenomPatient"));
        label_prenomPatient->setGeometry(QRect(490, 410, 46, 13));
        lineEdit_prenomPatient = new QLineEdit(centralWidget);
        lineEdit_prenomPatient->setObjectName(QString::fromUtf8("lineEdit_prenomPatient"));
        lineEdit_prenomPatient->setGeometry(QRect(590, 410, 191, 20));
        label_tempsRequete = new QLabel(centralWidget);
        label_tempsRequete->setObjectName(QString::fromUtf8("label_tempsRequete"));
        label_tempsRequete->setGeometry(QRect(470, 640, 161, 16));
        line_4 = new QFrame(centralWidget);
        line_4->setObjectName(QString::fromUtf8("line_4"));
        line_4->setGeometry(QRect(300, 40, 20, 621));
        line_4->setFrameShape(QFrame::VLine);
        line_4->setFrameShadow(QFrame::Sunken);
        listWidget_fichierPDF = new QListWidget(centralWidget);
        listWidget_fichierPDF->setObjectName(QString::fromUtf8("listWidget_fichierPDF"));
        listWidget_fichierPDF->setGeometry(QRect(10, 30, 281, 481));
        btn_deplacerTMP = new QPushButton(centralWidget);
        btn_deplacerTMP->setObjectName(QString::fromUtf8("btn_deplacerTMP"));
        btn_deplacerTMP->setGeometry(QRect(210, 550, 81, 23));
        comboBox_TMP = new QComboBox(centralWidget);
        comboBox_TMP->setObjectName(QString::fromUtf8("comboBox_TMP"));
        comboBox_TMP->setGeometry(QRect(10, 550, 191, 22));
        btn_supprimer = new QPushButton(centralWidget);
        btn_supprimer->setObjectName(QString::fromUtf8("btn_supprimer"));
        btn_supprimer->setGeometry(QRect(10, 580, 81, 23));
        btn_ouvrirDossier = new QPushButton(centralWidget);
        btn_ouvrirDossier->setObjectName(QString::fromUtf8("btn_ouvrirDossier"));
        btn_ouvrirDossier->setGeometry(QRect(200, 580, 91, 23));
        label_mutuelleEnCours = new QLabel(centralWidget);
        label_mutuelleEnCours->setObjectName(QString::fromUtf8("label_mutuelleEnCours"));
        label_mutuelleEnCours->setGeometry(QRect(10, 10, 341, 16));
        btn_actualiserPDF = new QPushButton(centralWidget);
        btn_actualiserPDF->setObjectName(QString::fromUtf8("btn_actualiserPDF"));
        btn_actualiserPDF->setGeometry(QRect(100, 580, 91, 23));
        checkBox_nonDeplacement = new QCheckBox(centralWidget);
        checkBox_nonDeplacement->setObjectName(QString::fromUtf8("checkBox_nonDeplacement"));
        checkBox_nonDeplacement->setGeometry(QRect(630, 480, 161, 20));
        radioButton_switch = new QRadioButton(centralWidget);
        radioButton_switch->setObjectName(QString::fromUtf8("radioButton_switch"));
        radioButton_switch->setGeometry(QRect(516, 550, 51, 20));
        lineEdit_mdpSWITCH = new QLineEdit(centralWidget);
        lineEdit_mdpSWITCH->setObjectName(QString::fromUtf8("lineEdit_mdpSWITCH"));
        lineEdit_mdpSWITCH->setGeometry(QRect(570, 550, 61, 20));
        btn_signalErreur = new QPushButton(centralWidget);
        btn_signalErreur->setObjectName(QString::fromUtf8("btn_signalErreur"));
        btn_signalErreur->setGeometry(QRect(360, 480, 81, 23));
        checkBox_multiSelection = new QCheckBox(centralWidget);
        checkBox_multiSelection->setObjectName(QString::fromUtf8("checkBox_multiSelection"));
        checkBox_multiSelection->setGeometry(QRect(10, 520, 91, 17));
        checkBox_selectionnerTout = new QCheckBox(centralWidget);
        checkBox_selectionnerTout->setObjectName(QString::fromUtf8("checkBox_selectionnerTout"));
        checkBox_selectionnerTout->setGeometry(QRect(330, 370, 41, 17));
        radioButton_ArchivageNT = new QRadioButton(centralWidget);
        radioButton_ArchivageNT->setObjectName(QString::fromUtf8("radioButton_ArchivageNT"));
        radioButton_ArchivageNT->setGeometry(QRect(650, 550, 111, 17));
        comboBox_autresMutuelles = new QComboBox(centralWidget);
        comboBox_autresMutuelles->setObjectName(QString::fromUtf8("comboBox_autresMutuelles"));
        comboBox_autresMutuelles->setGeometry(QRect(10, 610, 101, 22));
        btn_importerAutresMutuelles = new QPushButton(centralWidget);
        btn_importerAutresMutuelles->setObjectName(QString::fromUtf8("btn_importerAutresMutuelles"));
        btn_importerAutresMutuelles->setGeometry(QRect(150, 610, 141, 23));
        MainWindow->setCentralWidget(centralWidget);
        menuBar = new QMenuBar(MainWindow);
        menuBar->setObjectName(QString::fromUtf8("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 801, 21));
        menuEdition = new QMenu(menuBar);
        menuEdition->setObjectName(QString::fromUtf8("menuEdition"));
        menuAide = new QMenu(menuBar);
        menuAide->setObjectName(QString::fromUtf8("menuAide"));
        MainWindow->setMenuBar(menuBar);
        mainToolBar = new QToolBar(MainWindow);
        mainToolBar->setObjectName(QString::fromUtf8("mainToolBar"));
        MainWindow->addToolBar(Qt::TopToolBarArea, mainToolBar);
        statusBar = new QStatusBar(MainWindow);
        statusBar->setObjectName(QString::fromUtf8("statusBar"));
        MainWindow->setStatusBar(statusBar);

        menuBar->addAction(menuEdition->menuAction());
        menuBar->addAction(menuAide->menuAction());
        menuEdition->addAction(actionLecteur_PDF);
        menuAide->addAction(actionMise_jour);

        retranslateUi(MainWindow);

        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QApplication::translate("MainWindow", "MainWindow", 0, QApplication::UnicodeUTF8));
        actionLecteur_PDF->setText(QApplication::translate("MainWindow", "Lecteur PDF", 0, QApplication::UnicodeUTF8));
        actionMise_jour->setText(QApplication::translate("MainWindow", "Mise \303\240 jour", 0, QApplication::UnicodeUTF8));
        label_nomFichier->setText(QApplication::translate("MainWindow", "Nom fichier : ", 0, QApplication::UnicodeUTF8));
        radioButton_reconcilier->setText(QApplication::translate("MainWindow", "R\303\251concilier", 0, QApplication::UnicodeUTF8));
        radioButton_erreurDest->setText(QApplication::translate("MainWindow", "Erreur destinataire", 0, QApplication::UnicodeUTF8));
        radioButton_envoyerSEC->setText(QApplication::translate("MainWindow", "Envoyer en SEC", 0, QApplication::UnicodeUTF8));
        radioButton_ArchivageNR->setText(QApplication::translate("MainWindow", "Archivage NR", 0, QApplication::UnicodeUTF8));
        btn_executer->setText(QApplication::translate("MainWindow", "R\303\251concilier", 0, QApplication::UnicodeUTF8));
        label_emetteur_DPEC->setText(QApplication::translate("MainWindow", "Emetteur DPEC : ", 0, QApplication::UnicodeUTF8));
        label_dest_DPEC->setText(QApplication::translate("MainWindow", "Destinataire DPEC :", 0, QApplication::UnicodeUTF8));
        label_dateEntree->setText(QApplication::translate("MainWindow", "Date d'entr\303\251e :", 0, QApplication::UnicodeUTF8));
        label_nomPatient->setText(QApplication::translate("MainWindow", "Nom :", 0, QApplication::UnicodeUTF8));
        label_numDossier->setText(QApplication::translate("MainWindow", "Num\303\251ro dossier :", 0, QApplication::UnicodeUTF8));
        label_recherche->setText(QApplication::translate("MainWindow", "Recherche :", 0, QApplication::UnicodeUTF8));
        label_IDMsg->setText(QApplication::translate("MainWindow", "ID Message :", 0, QApplication::UnicodeUTF8));
        label_NNI->setText(QApplication::translate("MainWindow", "Num\303\251ro de S\303\251cu :", 0, QApplication::UnicodeUTF8));
        label_nomPatient_rche->setText(QApplication::translate("MainWindow", "Nom patient :", 0, QApplication::UnicodeUTF8));
        label_nomMutuelle->setText(QApplication::translate("MainWindow", "Mutuelle :", 0, QApplication::UnicodeUTF8));
        btn_rechercher->setText(QApplication::translate("MainWindow", "Rechercher", 0, QApplication::UnicodeUTF8));
        label_nomEDS->setText(QApplication::translate("MainWindow", "EDS :", 0, QApplication::UnicodeUTF8));
        label_numDossier_rche->setText(QApplication::translate("MainWindow", "Num\303\251ro dossier :", 0, QApplication::UnicodeUTF8));
        label_statutDPEC->setText(QApplication::translate("MainWindow", "Statut DPEC :", 0, QApplication::UnicodeUTF8));
        checkBox_multiReconciliation->setText(QApplication::translate("MainWindow", "Multi-R\303\251conciliation", 0, QApplication::UnicodeUTF8));
        label_zoneErreur->setText(QApplication::translate("MainWindow", "Zone erreur :", 0, QApplication::UnicodeUTF8));
        label_statutRPEC->setText(QApplication::translate("MainWindow", "Statut RPEC :", 0, QApplication::UnicodeUTF8));
        label_nbDPEC->setText(QString());
        btn_importerEDS->setText(QApplication::translate("MainWindow", "Importer EDS", 0, QApplication::UnicodeUTF8));
        btn_telechargerDPEC->setText(QApplication::translate("MainWindow", "T\303\251l\303\251charger DPEC", 0, QApplication::UnicodeUTF8));
        btn_telechargerRPEC->setText(QApplication::translate("MainWindow", "T\303\251l\303\251charger RPEC", 0, QApplication::UnicodeUTF8));
        label_prenomPatient->setText(QApplication::translate("MainWindow", "Pr\303\251nom :", 0, QApplication::UnicodeUTF8));
        label_tempsRequete->setText(QString());
        btn_deplacerTMP->setText(QApplication::translate("MainWindow", "D\303\251placer", 0, QApplication::UnicodeUTF8));
        btn_supprimer->setText(QApplication::translate("MainWindow", "Supprimer", 0, QApplication::UnicodeUTF8));
        btn_ouvrirDossier->setText(QApplication::translate("MainWindow", "Ouvrir dossier", 0, QApplication::UnicodeUTF8));
        label_mutuelleEnCours->setText(QString());
        btn_actualiserPDF->setText(QApplication::translate("MainWindow", "Actualiser", 0, QApplication::UnicodeUTF8));
        checkBox_nonDeplacement->setText(QApplication::translate("MainWindow", "Ne pas d\303\251placer apr\303\250s envoi", 0, QApplication::UnicodeUTF8));
        radioButton_switch->setText(QApplication::translate("MainWindow", "Switch", 0, QApplication::UnicodeUTF8));
        btn_signalErreur->setText(QApplication::translate("MainWindow", "Signaler erreur", 0, QApplication::UnicodeUTF8));
        checkBox_multiSelection->setText(QApplication::translate("MainWindow", "Multi-s\303\251lection", 0, QApplication::UnicodeUTF8));
        checkBox_selectionnerTout->setText(QApplication::translate("MainWindow", "Tout", 0, QApplication::UnicodeUTF8));
        radioButton_ArchivageNT->setText(QApplication::translate("MainWindow", "Archivage NT", 0, QApplication::UnicodeUTF8));
        btn_importerAutresMutuelles->setText(QApplication::translate("MainWindow", "Importer autres mutuelles", 0, QApplication::UnicodeUTF8));
        menuEdition->setTitle(QApplication::translate("MainWindow", "Edition", 0, QApplication::UnicodeUTF8));
        menuAide->setTitle(QApplication::translate("MainWindow", "Aide", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
