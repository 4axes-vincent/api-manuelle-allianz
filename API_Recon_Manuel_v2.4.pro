#-------------------------------------------------
#
# Project created by QtCreator 2015-05-12T09:19:13
#
#-------------------------------------------------

QT += core gui

QT += network

QT += webkit

QT += sql

QT += xml

TARGET = API_Recon_Manuel
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    dial_bdd.cpp \
    configuration.cpp \
    dialog_lecteurpdf.cpp \
    dialog_nontraitable.cpp \
    form_stats.cpp \
    qcustomplot.cpp

HEADERS  += mainwindow.h \
    dial_bdd.h \
    configuration.h \
    dialog_lecteurpdf.h \
    dialog_nontraitable.h \
    form_stats.h \
    qcustomplot.h

FORMS    += mainwindow.ui \
    dialog_lecteurpdf.ui \
    dialog_nontraitable.ui \
    form_stats.ui

RC_FILE = ressource.rc
