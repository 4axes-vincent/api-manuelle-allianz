#ifndef FORM_STATS_H
#define FORM_STATS_H

#include <QWidget>
#include <QString>
#include <QStringBuilder>
#include <QFile>
#include <QDir>
#include <QList>
#include <QVector>
#include <QMessageBox>

#include "dial_bdd.h"

namespace Ui {
class Form_stats;
}

class Form_stats : public QWidget
{
    Q_OBJECT
    
public:
    explicit Form_stats(QWidget *parent = 0);
    ~Form_stats();

    void debugger(QString titre, QString information);
    void genererStats(QString date, QString typeAPI, QString contexte);
    void genererStats(QString dateDebut, QString dateFin, QString typeAPI, QString contexte);
    void afficherGraphPoint (QVector<double> vecteurY);

private slots:

    void on_btn_genererStats_clicked();

    void on_calendrier_clicked(const QDate &date);

    void on_checkBox_journee_clicked(bool checked);

    void on_checkBox_periode_clicked(bool checked);

    void on_btn_exporter_clicked();
    
private:
    Ui::Form_stats *ui;

    int clickID;
    QString contexte;

    float moyenne;
};

#endif // FORM_STATS_H
