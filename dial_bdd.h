#ifndef DIAL_BDD_H
#define DIAL_BDD_H

#include <QtSql/QSqlDatabase>
#include <QtSql/QSqlError>
#include <QtSql/QSqlQuery>
#include <QtSql/QSqlResult>
#include <QtSql/QSqlRecord>

#include "configuration.h"

#include <QCoreApplication>
#include <QHostInfo>
#include <QMessageBox>
#include <QStringBuilder>
#include <QDateTime>

class Dial_bdd
{
public:
    Dial_bdd(QString baseName);

    void debuggerInfo(QString titre, QString msg);
    void majDonnee(QString user, QString contexte, int stats);
    void ajouterErreurDest(QString user, int stats);
    QVector<double> importerStats(QString contexte, QString typeAPI, QString date);
    QVector<double> importerStats(QString contexte, QString typeAPI, QString dateDebut, QString dateFin);
    void enregistrerStats(QString date, QString contexte, QString api, QString temps);
    void enregistrerReconciliation(QString nomFichier, QString idMessage, QString operatrice, QString vm, QString action, QString nomP,
                                   QString prenomP, QString numDossier, QString dateE, QString CH, QString mutuelleDpec, QString mutuelleVm);

private:

    Configuration *config;

    QSqlDatabase db;
    QString bddBaseName;
};

#endif // DIAL_BDD_H
